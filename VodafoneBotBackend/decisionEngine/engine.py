from RuleEngine import Engine as RuleExecutor
from RuleEngine import ActionManager, ResponseManager
from PaxcelChatBotFramework import Engine as ChatBotRuleEngine
from .contextBuilder import contextBuilder
from .ruleManager import RuleManager
from PaxcelChatBotFramework.Models import RuleEngineOutput, VisitedNode, Context, Output
from PaxcelChatBotFramework.Enums import RedirectType
from vodafoneApi import ApiFactory
from .response import *
from .actions import *
import datetime
import logging
logger = logging.getLogger(__name__)


class DecisionEngine(ChatBotRuleEngine):
    @staticmethod
    def Initialize():
        ResponseManager.add_action("text", textResponse)
        ResponseManager.add_action("options", optionResponse)
        ResponseManager.add_action("form", formResponse)
        ResponseManager.add_action("conditional", conditionalResponse)
        ResponseManager.add_action("jumpto", jumptoResponse)
        ActionManager.add_action("addToContext", addToContextAction)
        ActionManager.add_action("addEntitytocontext", addEntitytocontext)
        ActionManager.add_action("anythingElseContext", insertUnidentifiedIntents)
        ActionManager.add_action("checkParamsBySelectedCriteria", checkParamsBySelectedCriteria)
        ActionManager.add_action("checkParamsByOtherCriteria", checkParamsByOtherCriteria)
        ActionManager.add_action("checkParamsByPerticularCriterias", checkParamsByPerticularCriterias)
        ActionManager.add_action("checkParamsBySchoolBasedCriterias", checkParamsBySchoolBasedCriterias)
        
        apiFactory = ApiFactory()
        ActionManager.add_action("APICALL", apiFactory.apiCall)

    def __init__(self):
        pass

    def Process(self, botResponse):
        try:
            start = datetime.datetime.now()
            ruleLoader = RuleManager()
            ruleExec = RuleExecutor(ruleLoader)
            contextTable = contextBuilder(botResponse)
            resp = ruleExec.Execute(contextTable)
            result = RuleEngineOutput()
            result.AdditionalData = botResponse.AdditionalData
            result.Context = self.__prepareContext__(
                botResponse.Context, resp["context"])
            result.Output = self.__prepareOutput__(resp["output"])
            stop = datetime.datetime.now()
            totalTime = "Total time used by rule engine:" + str(stop - start)
            logger.info(
                "User Query: {0}".format(totalTime)
            )
        except:
            logger.info(
                "User Query: {0}".format("Exception occurd in rule engine")
            )
            raise
        return result

    def __prepareContext__(self, oldContext, engineContext):
        nodes = engineContext["nodes"]
        oldContext.Properties = {}
        oldContext.VisitedNodes = []
        for node in nodes:
            vnode = VisitedNode()
            if isinstance(node, VisitedNode):
                vnode = node
                vnode.RedirectType = vnode.RedirectType
            else:
                vnode.NodeId = node["nodeid"]
                vnode.Name = node["name"]
                vnode.RedirectType = node["redirectType"]
                vnode.RedirectFrom = node.get("redirectFrom", None)

            oldContext.VisitedNodes.append(vnode)

        for prop in engineContext:
            oldContext.Properties[prop] = engineContext[prop]
        return oldContext

    def __prepareOutput__(self, responses):
        result = []
        for response in responses:
            result.append(response)

        return result
