def contextBuilder(botResponse):
    table = {
        "$nodes": []
    }
    if botResponse.Intent is not None:
        table["#intent"] = botResponse.Intent.Name
    for entity in botResponse.Entities:
        entityName = "@" + entity.Name
        if (entityName) not in table:
            table[entityName] = []
        table[entityName].append(entity.Value)

    dbConfig = __fetchDBValues__(botResponse.AdditionalData)
    table['dbConfig']=dbConfig
    context = botResponse.Context
    table["$nodes"] = context.VisitedNodes
    for key in context.Properties.keys():
        table['$' + str(key)] = context.Properties[key]

    return table

def __fetchDBValues__(additionalData):
    config = {}
    config = {"$msg": additionalData["message"],
                "$uuid": additionalData["uuid"], "$logId": additionalData["logId"]}
    return config