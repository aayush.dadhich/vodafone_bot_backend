from businessLogicLayer import GetScholarshipParams
def checkParamsBySelectedCriteria(input, context):

    payload=GetScholarshipParams()
    check=payload.checkParamsBySelectedCriteria(context)
    
    setParam(check,context)
    data=formatResponse()
    return data

def checkParamsByOtherCriteria(input, context):

    payload=GetScholarshipParams()
    check=payload.checkParamsByOtherCriteria(context)

    setParam(check,context)
    data=formatResponse()
    return data

def setParam(check,context):

    if check:
        context["~isParam"]="True"
    else:
        context["~isParam"]="False"

def checkParamsByPerticularCriterias(input, context):

    payload=GetScholarshipParams()
    context["$Subject"]=payload.getDefaultSubject(context)
    data=checkParamsByOtherCriteria(input, context)
    return data

def checkParamsBySchoolBasedCriterias(input, context):
    
    payload=GetScholarshipParams()
    context["$Classnlp"]=payload.getDefaultClass(context)
    data=checkParamsByOtherCriteria(input, context)
    return data

def formatResponse():
    
    dict={}
    data = dict
    return data