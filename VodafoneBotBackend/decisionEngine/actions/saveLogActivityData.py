import json
from vodafoneApi import SaveUserActivityResponse

def savelogActivity(_input, arg, context):

    execute=SavelogActivity(_input, arg)
    response=execute.execute(context)
    
    return response

class SavelogActivity(SaveUserActivityResponse):

    def getRequestPayLoad(self,context):

        params = {
        "eventType": self._input,
        "entityName": "Chatbot",
        "entityId": self.arg,
        "userId": context
        }
        return params

    def formatSuccessResponse(self, response, context): 

        response = response.json()
        return response