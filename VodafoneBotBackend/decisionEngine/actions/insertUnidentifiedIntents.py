from dataAccessLayer.dbHandler import insertQuery
import datetime

def insertUnidentifiedIntents(input, context):
    
    dict={}
    payload=getRequestPayload(context)
    insertQuery(payload,'unidentified_intents')
    return dict

def getRequestPayload(context):

    date=datetime.datetime.now()
    message=context["dbConfig"]["$msg"]
    uuid=context["dbConfig"]["$uuid"]
    logId=context["dbConfig"]["$logId"]
    payload={"intent": message,"uuid":uuid,"conversationid":logId,"created_time":date.strftime("%Y-%m-%d")}

    return payload