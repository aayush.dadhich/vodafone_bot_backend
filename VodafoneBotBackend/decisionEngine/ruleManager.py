from RuleEngine.Components import RuleLoader
from dataAccessLayer.dbHandler import getRules, getRuleById, getAnyThingElseNode, hasChildNode


class RuleManager(RuleLoader):

    def getRules(self, parentId) -> list:
        return getRules(parentId)

    def getRuleById(self, id):
        return getRuleById(id)

    def getAnyThingElseNode(self):
        return getAnyThingElseNode()

    def hasChildNode(self, parentId):
        return hasChildNode(parentId)
    
    def getGuards(self) -> list:
        list=[]
        return list
