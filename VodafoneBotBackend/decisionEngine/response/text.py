from PaxcelChatBotFramework.Models import Output
from RuleEngine import StringBuilder
def textResponse(input, context):
    result = {
        "action": "return"
    }
    output = Output()
    output.Type = "text"
    output.Message = StringBuilder(input, context)
    result["output"] = output
    return result




