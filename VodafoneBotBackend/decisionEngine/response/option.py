from PaxcelChatBotFramework.Models import Output

def optionResponse(input, context):
    options = []
    text = input["text"]
    option = input["options"]
    if type(option) is str:
        if input in context:
            options = context[input]
        else:
            options.append(input)
    elif type(option) is list:
        options = input
    output = Output()
    output.Data = {
        "text": text,
        "options": options
        }
    output.Type = "option"
    result = {
        "action": "return",
        "output": output
    }
    return result
