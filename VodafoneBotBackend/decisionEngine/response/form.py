from PaxcelChatBotFramework.Models import Output
from RuleEngine import JsonBuilder
def formResponse(inputValue, context):
    data = {}
    if "data" in inputValue:
        data = JsonBuilder(inputValue["data"], context)
    output = Output()
    output.Type = "form"
    output.Data = {
        "name": inputValue["name"],
        "data": data
    }
    result = {
        "action": "return",
        "output": output
    }
    return result

def clearContext(context):
    if "scholarship" in context:
        del context["scholarship"]

