from RuleEngine import ResponseManager, evaluator, Response

def conditionalResponse(inputValue, context):

    for item in inputValue:
        if evaluator(item["condition"], context):
            resp = item["response"]
            responses = []
            if isinstance(resp, list):
                for rsp in resp:
                    responses.append(Response(rsp))
            else:
                responses.append(Response(resp))
            output = []

            for response in responses:
                value = ResponseManager.call_action(response.Type, {
                    "data": response.Value,
                    "context": context
                })

                if response.Type == "jumpto":
                    return value
                output.append(value["output"])

            return {
                "action": "return",
                "output": output
            }

    return {
        "action": "return",
        "output": []
    }