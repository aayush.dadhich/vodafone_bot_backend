import datetime
import logging

logger = logging.getLogger(__name__)


def startlogging():
    start = datetime.datetime.now()
    return(start)
        

def stoplogging(start,message):
    stop = datetime.datetime.now()
    totalTime = message + str(stop - start)
    logger.info("User Query: {0}".format(totalTime))

