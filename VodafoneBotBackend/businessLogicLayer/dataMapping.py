class DataMapping():

    def getSchList(apidata, count, language):
        sch_list = {
                        "id": apidata['data'][count]["_id"],
                        "publish_later": apidata['data'][count]["publish_later"],
                        "valid_till": apidata['data'][count]["valid_till"],
                        "apply_online_link": apidata['data'][count]["apply_online_link"],
                        "media_url": apidata['data'][count]["media_url"],
                        "listitem": apidata['data'][count]['content_by_language'][language],
                        "slug": apidata['data'][count]["slug"]
                    }

        return sch_list
