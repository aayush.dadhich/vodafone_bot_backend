from . import ParamsHelper

class GetScholarshipParams(ParamsHelper):

    def getParamsBySelectedCriteria(self,context):
    
        if "$Class" in context:
            clas = context["$Class"][0] 
        else:
            clas = []
        if "$Gender" in context:
            gender = context["$Gender"][0]
        else:
            gender = []
        if "$Category" in context:
            category = context["$Category"][0]
        else:
            category = []
        if "$Preference" in context:
            preference = context["$Preference"][0]
        else:
            preference = []
        language=context.get("$Language",self.getDefaultLanguage())
        param = {
            "Class": clas,
            "Gender": gender,
            "Category": category,
            "Preference": preference,
            "Language": language
        }
        return param

    def getParamsByOtherCriteria(self,context):

        if "$Classnlp" in context:
            clas = context["$Classnlp"]
        else:
            clas = []
        if "$Gendernlp" in context:
            gender = context["$Gendernlp"]
        else:
            gender = []
        if "$Categorynlp" in context:
            category = context["$Categorynlp"]
        else:
            category = []
        if "$Preferencenlp" in context:
            preference = context["$Preferencenlp"]
        else:
            preference = []

        if "$Age" in context:
            age = context["$Age"]
        else:
            age = []

        if "$Subject" in context:
            subject = context["$Subject"]
        else:
            subject = []

        if "$State" in context:
            state = context["$State"]
        else:
            state = []

        if "$AnnualFamilyIncome" in context:
            annualFamilyIncome = context["$AnnualFamilyIncome"]
        else:
            annualFamilyIncome = []

        if "$Religion" in context:
            religion = context["$Religion"]
        else:
            religion = []
        
        language=context.get("$Language",self.getDefaultLanguage())
        param = {
            "Class": clas,
            "Gender": gender,
            "Category": category,
            "Preference": preference,
            "Age": age,
            "Subject": subject,
            "State": state,
            "AnnualFamilyIncome": annualFamilyIncome,
            "Religion": religion
        }
        return param
    
#check params avail from here

    def checkParamsBySelectedCriteria(self,context):

        param=self.getParamsBySelectedCriteria(context)
        if not param["Class"] and not param["Gender"] and not param["Category"] and not param["Preference"]:
            check=False
        else:
            check=True

        return check
    
    def checkParamsByOtherCriteria(self,context):
   
        param=self.getParamsByOtherCriteria(context)
        if not param["Class"] and not param["Gender"] and not param["Category"] and not\
        param["Preference"] and not param["Age"] and not param["Subject"] and not param["State"] and not\
        param["AnnualFamilyIncome"] and not param["Religion"]:
            check=False
        else:
            check=True
        return check