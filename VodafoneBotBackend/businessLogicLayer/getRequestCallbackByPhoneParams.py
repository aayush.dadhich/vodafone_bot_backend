from dataAccessLayer.dbHandler import conversationHistory

class GetRequestCallbackByPhoneParams:

    def getParams(self,context,setParams):
        stringlogs=self.getStringLogs(context)

        if "$name" in context:
            namerequested = context["$name"][0]
        else:
            namerequested = context["$reqname"][0]

        if "$phoneno" in context:
            phoneNo = context["$phoneno"][0]
        else:
            phoneNo = context["$reqphoneno"][0]

        if "$email" in context:
            email = context["$email"][0]
        else:
            email = ""
        query = stringlogs
        communicationmedium = context["$reqphoneno"][0]

        if setParams=="updateQuery":
            setParam = {
            "ticketId":context["$submitquery"]["ticket"],
            "communicationMode": "Mobile",
            "communicationMedium": phoneNo
            }

        elif setParams=="addQuery":
            setParam= {
                "name": namerequested,
                "email": email,
                "mobile": phoneNo,
                "query": query,
                "source": "Chatbot",
                "host":"Query",
                "communicationMode": "Mobile",
                "communicationMedium": communicationmedium

                }
        return setParam
    
    def getStringLogs(self,context):

        convid=context["$logsId"]
        arraylogs=[]
        conversation=conversationHistory(convid)
        for count in range (0,len(conversation)):
            _conversation=conversation[count]['message']
            arraylogs.append(_conversation[0]['message'])
            stringlogs='<br>'.join(arraylogs)
        return stringlogs