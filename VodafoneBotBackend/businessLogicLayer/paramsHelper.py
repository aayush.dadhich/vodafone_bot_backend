import os,json
from VodafoneBotBackend import settings

config=settings.config

class ParamsHelper:
    
    def getDefaultClass(self,context):
        return config["defaultApiValues"]["class"]
    
    def getSubject(self,context):
        return context.get("$Subject",None)
    
    def getDefaultSubject(self,context):
        return config["defaultApiValues"]["subject"]

    def getLanguage(self,context):
        language=context.get("$Language",config["defaultApiValues"]["language"])
        return language[0]

    def getDefaultLanguage(self):
        return config["defaultApiValues"]["language"]

    def getEmail(self,context):
        return context["$email"][0]

    def getParamsByLanguage(self,context):

        params = {"language": self.getLanguage(context)}
        return params
    
    def getParamsByEmail(self,context):
        
        params = {"Email": self.getEmail(context)}
        return params