from dataAccessLayer.dbHandler import conversationHistory

class GetRequestCallbackByEmailParams:

    def getParams(self,context,setParams):
        stringlogs=self.getStringLogs(context)
        
        if "$name" in context:
            namerequested = context["$name"][0]
        else:
            namerequested = context["$reqname"][0]

        if "$phoneno" in context:
            phoneNo = context["$phoneno"][0]
        else:
            phoneNo = ""

        if "$email" in context:
            email = context["$email"][0]
        else:
            email = context["$reqemail"][0]
        query = stringlogs
        communicationmedium = context["$reqemail"][0]
        query = "<p>"+stringlogs+"</p>"
    
        if setParams=="updateQuery":
            setParam = {
                "ticketId":context['$submitquery']["ticket"],
                "communicationMode": "Email",
                "communicationMedium": communicationmedium
            }
        if setParams=="addQuery":
            setParam = {
                    "name": namerequested,
                    "email": email,
                    "mobile": phoneNo,
                    "query": query,
                    "source": "Chatbot",
                    "host":"Query",
                    "communicationMode": "Email",
                    "communicationMedium": communicationmedium
                }
        return setParam
    
    def getStringLogs(self,context):

        convid=context["$logsId"]
        arraylogs=[]
        conversation=conversationHistory(convid)
        for count in range (0,len(conversation)):
            _conversation=conversation[count]['message']
            arraylogs.append(_conversation[0]['message'])
            stringlogs='<br>'.join(arraylogs)
        return stringlogs