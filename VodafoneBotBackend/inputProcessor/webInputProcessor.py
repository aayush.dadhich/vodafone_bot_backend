import logging
import json
import datetime
import random
from PaxcelChatBotFramework import InputProcessor
from PaxcelChatBotFramework.Models import Request
from dataAccessLayer.dbHandler import insertChatlogs, updateChatlogs
from VodafoneBotBackend import setup
from logger.performanceLogger import startlogging, stoplogging
from decisionEngine.actions.saveLogActivityData import savelogActivity

logger = logging.getLogger(__name__)


class WebInputProcessor(InputProcessor):

    @staticmethod
    def CanHandle(request) -> bool:

        return request["additionalData"]["source"] == "web"

    def Process(self, request):

        start = startlogging()
        received_json_data = request
        sresult = 'step1' + json.dumps(received_json_data)
        logger.info(
            "User Query: {0}".format(sresult)
        )

        inputMessage=self.getinputMessage(received_json_data)
        conversationId=self.getconversationId(received_json_data)
        userId=self.getuserId(received_json_data)
        self.addConversationLog(request,inputMessage,conversationId,userId)
        request = self.setRequest(received_json_data)
        Result = Request(request)

        stoplogging(start, "Total time for process input :")
        logger.info("User Query: {0}".format(
            ('result got from web Input Processor')))
        return Result
    
    def setRequest(self,received_json_data):

        _type = received_json_data["type"]
        _input = received_json_data["input"]

        additionalData = received_json_data["additionalData"]
        additionalData["message"] = received_json_data["input"]
        additionalData["logId"] = received_json_data["context"]["logsId"]
        additionalData["uuid"] = received_json_data["context"]["conversationId"]
        _context = received_json_data["context"]

        request = {"type": _type, "context":_context, "additionalData": additionalData,
                   "input":_input}
        
        return request
    
    
    def addConversationLog(self,request,inputMessage,conversationId,userId):

        convstarttime = datetime.datetime.now()
        chatlogsId = insertChatlogs(inputMessage, conversationId, str(
            userId), str(convstarttime), False)
        request["context"]["logsId"] = chatlogsId

        chatlogsUpdatedId = updateChatlogs(conversationId, str(userId))
        request["context"]["UpdatedUserId"] = chatlogsUpdatedId


    def getinputMessage(self,received_json_data):

        if "message" in received_json_data['type']:
            inputMessage = received_json_data['input']['message']
        else:
            if '@text' in received_json_data['input']['data']:
                inputMessage = received_json_data['input']['data']['@text']
            else:
                inputMessage = received_json_data['input']['data']['#Name']
        return(inputMessage)

    def getconversationId(self,received_json_data):

        if "conversationId" not in received_json_data["context"]:
            newConv = random.random()
            received_json_data["context"]["conversationId"] = str(newConv)

        conversationId = received_json_data["context"]["conversationId"]
        return(conversationId)

    def getuserId(self,received_json_data):

        if 'uid' in received_json_data['context']:
            userId = received_json_data['context']['uid']
            self.saveLogs(received_json_data)
        else:
            userId = setup.config['defaultUserVariables']['userId']
        return(userId)
    
    def saveLogs(self,received_json_data):
        
        if 'loginActiveLog' not in received_json_data["context"]:
            currentOption = {"#report_problem_data": "REPORTPROBLEM", "#best_scholarships_signin_profile_scholarships": "BESTSCHOLARSHIP",
                            "#claimed_scholarships_scholarships_available_check": "CLAIM",
                            "#subscribe_afterOTP_check": "SUBSCRIBE",
                            "#get_claimed_scholarships_afterOTP_check": "GETCLAIMED",
                            "#signin_afterOTP_profile_check": "APPLY",
                            '#best_scholarships_signup_profile_scholarships':"BESTSCHOLARSHIP",
                            '#signup_afterOTP_profile_check':'APPLY'}
            received_json_data["context"]["loginActiveLog"] = setup.config['defaultUserVariables']['loginActiveLog']
            savelogActivity("LOGIN" if received_json_data["context"]["isAuthCheck"] == 'true' else 'REGISTRATION', 
                currentOption[received_json_data['input']['data']['#Name']], 
                received_json_data["context"]["email"][0] if received_json_data["context"]["isAuthCheck"] == 'true' else received_json_data["input"]["data"]['@email'])
