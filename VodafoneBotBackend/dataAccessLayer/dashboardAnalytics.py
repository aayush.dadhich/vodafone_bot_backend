import logging
import datetime
from .dbConnectionPooling import getDb

logger = logging.getLogger(__name__)

def dateGeneration(startDate, EndDate, result, apiData, apiDate):
    start = datetime.datetime.strptime(startDate, '%Y-%m-%d')
    end = datetime.datetime.strptime(EndDate, '%Y-%m-%d')
    step = datetime.timedelta(days=1)
    tempArray = []
    while start <= end:
        date = start.date()
        start += step
        newDate = date.strftime("%Y-%m-%d")
        flag = False
        if result is not None:
            for data in result:
                if data is not None and data['_id'][apiDate] == newDate: 
                    flag = True
                    tempObj = {apiData : data[apiData] , apiDate : newDate}
                    tempArray.append(tempObj)
                    break
        if(flag == False):
            tempObj = { apiData : 0 , apiDate : newDate}
            tempArray.append(tempObj)
        flag = False
    return tempArray

def aggregateQuery(matchData,findData,countData,collectionName):
    collection = getDb()[collectionName]
    result=list(collection.aggregate([{
                                    "$match":matchData},
                                    {"$group":{
                                        "_id":findData,
                                        "getby": {"$addToSet":countData["getCountBy"]},
                                        
                                    }},
                                    {
                                        "$project":{
                                            "_id":1,
                                            countData["countAs"]:{"$size": "$getby"}

                                        }
                                    }

                                    ]))
    return result


def getUserByDate(startDate, EndDate):
    matchData={"addedon":{ "$gt":startDate, "$lte":EndDate}}
    countData={"getCountBy":"$conversationid","countAs":"usercount"}
    collectionName='userconversationtimedetails'
    findData={"startdate":"$addedon"}

    result=aggregateQuery(matchData,findData,countData,collectionName)
    resultData = dateGeneration(startDate, EndDate, result, "usercount", "startdate")
    return resultData


def getMsgByDate(startDate, EndDate):
    matchData={"addedon":{ "$gt":startDate, "$lte":EndDate}}
    countData={"getCountBy":"$messageid","countAs":"msgcount"}
    collectionName='sequencedetails'
    findData={"startdate":"$addedon"}

    result=aggregateQuery(matchData,findData,countData,collectionName)
    resultData = dateGeneration(startDate, EndDate, result, "msgcount", "startdate")
    return resultData

def getConvByDate(startDate, EndDate):
    matchData={"addedon":{ "$gt":startDate, "$lte":EndDate}}
    countData={"getCountBy":"$userid","countAs":"conversations"}
    collectionName='userconversationtimedetails'
    findData={"startdate":"$addedon"}

    result=aggregateQuery(matchData,findData,countData,collectionName)
    resultData = dateGeneration(startDate, EndDate, result, "conversations", "startdate")
    return resultData

def getLeadGeneratedByDate(startDate, EndDate):
    matchData={"$and":[{"addedon":{ "$gt":startDate, "$lte":EndDate}},{"userid":{"$ne":"0"}}]}
    countData={"getCountBy":"$conversationid","countAs":"leadcount"}
    collectionName='userconversationtimedetails'
    findData={"startdate":"$addedon"}

    result=aggregateQuery(matchData,findData,countData,collectionName)
    resultData = dateGeneration(startDate, EndDate, result, "leadcount", "startdate")
    return resultData

def getUnidentifiedIntent(startDate, EndDate):
    data = {"startDate":startDate,"EndDate":EndDate}
    collectionName='unidentified_intents'
    collection = getDb()[collectionName]

    result=list(collection.aggregate([{"$match":{"created_time":{ "$gt":data["startDate"], "$lte":data["EndDate"]}}},
                                    {"$project":{"created_time":1,"intent":1,"_id":0}}]))
    if result == None:
        result = []
    return result


