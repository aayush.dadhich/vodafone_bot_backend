import logging
import datetime
from .dbConnectionPooling import getDb
import hashlib
LOGGER = logging.getLogger(__name__)

def getRules(parentId):

    if parentId is None:
        parentId = ""
    return getNodeList({"parent_dialog_id": parentId})


def getRuleById(ruleId):

    return getNodeList({"node_id": ruleId})[0]


def getAnyThingElseNode():

    node = getNodeList({"name": "anything_else"})[0]
    return node

def hasChildNode(parentId):

    nodes = getNodeList({"parent_dialog_id": parentId})
    return len(nodes) != 0

def getNodeList(query):

    ruleList = []
    collection = getDb()["vodafone_master_dialog"]
    node = None
    for node in list(collection.find(query)):
        ruleList.append(formatNode(node))
    return ruleList

def formatNode(node):

    formatedRule = {
        "nodeid": node["node_id"],
        "condition": node["condition"],
        "slots": node["slots"],
        "actions": node["actions"],
        "response": node["response"],
        "parent_dialog_id": node["parent_dialog_id"],
        "sibling_dialog_id": node["sibling_dialog_id"],
        "name": node["name"],
    }
    return formatedRule

def findQuery(findData,projectData,collectionName):

    collection = getDb()[collectionName]
    result=list(collection.find(findData,projectData))
    return result


def insertQuery(data,collectionName):

    collection = getDb()[collectionName]
    collection.insert_one(data)

def updateQuery(findData,replaceData,_upsert,collectionName):

    collection = getDb()[collectionName]
    collection.update(findData,replaceData,upsert=True)

def hashEncoding(userMessage):

    m = hashlib.md5()
    m.update(userMessage.encode('utf-8'))
    return(m.hexdigest())

def userConversationTimeDetails(uuid,userid,convstarttime,date):

    conversationid=hashEncoding(convstarttime)
    collectionName='userconversationtimedetails'

    findData={"uuid":uuid}
    updateChatlogs={"$setOnInsert":{"conversationid":conversationid,"uuid":uuid,"userid": userid,
                    "addedon" : date.strftime("%Y-%m-%d"),"convstarttime": convstarttime}}
    projectData={"_id":0,"conversationid":1}
    _upsert=True

    updateQuery(findData,updateChatlogs,_upsert,collectionName)
    result=findQuery(findData,projectData,collectionName)
    _conversationid=result[0]
    return(str(_conversationid["conversationid"]))
    
def messagesChatlog(userMessage,convstarttime):

    _messagechecksum=hashEncoding(userMessage)
    messageid=hashEncoding(convstarttime)
    collectionName='messages'

    findData={"messagechecksum":_messagechecksum}
    updateChatlogs={"$setOnInsert":{"messageid":messageid,"message":userMessage,"messagechecksum":_messagechecksum}}
    projectData={'_id':0,"messageid":1}
    _upsert=True

    updateQuery(findData,updateChatlogs,_upsert,collectionName)
    result=findQuery(findData,projectData,collectionName)
    _messageid=result[0]
    return(str(_messageid['messageid']))

def messagesSequenceChatlogs(_conversationid,_messageid,isbot,date):

    collectionName='sequencedetails'

    result=getLastSequenceNo(collectionName,_conversationid)
    _seqno=getNewSequenceNo(result)

    data={"conversationid":_conversationid,"seqno":_seqno,"messageid":_messageid,
                            "isbot":isbot,"addedon" : date.strftime("%Y-%m-%d")}
    insertQuery(data,collectionName)

def getLastSequenceNo(collectionName,_conversationid):

    collection = getDb()[collectionName]
    result=list(collection.find({"conversationid":_conversationid}, {"seqno": 1, "_id":0}).sort("seqno",-1).limit(1))
    return result

def getNewSequenceNo(result):

    if(len(result)==0):
        _seqno=1
    else:
        _seqno=result[0]
        _seqno=_seqno["seqno"]+1
    return(_seqno)

def insertChatlogs(userMessage,uuid,userid,convstarttime,isbot): 

    date=datetime.datetime.now()
    _conversationid=userConversationTimeDetails(uuid,userid,convstarttime,date)
    _messageid=messagesChatlog(userMessage,convstarttime)
    messagesSequenceChatlogs(_conversationid,_messageid,isbot,date)

    return(_conversationid)

def insertFeedback(conversationidvalue,convstarttime,feedbackdata):

    collectionName='userconversationtimedetails'
    findData={"uuid":uuid}
    projectData={"_id":0,"conversationid":1}
    result=findQuery(findData,projectData,collectionName)
    _conversationid=result[0]
    feedback = {
        "conversationid":_conversationid,
        "uuid": conversationidvalue,
        "_convstarttime": convstarttime,
        "feedbackdata": feedbackdata
    }
    collectionName='vodafone_feedback'

    insertQuery(feedback,collectionName)
    chatlogsId=_conversationid
    return chatlogsId


def updateChatlogs(coversationidvalue, useridvalue):

    findData={"uuid": coversationidvalue}
    updateChatlogs = {"$set": {"uuid": coversationidvalue,"userid": useridvalue}}
    projectData={"_id":0,"userid":1}
    collectionName='userconversationtimedetails'
    _upsert=False

    updateQuery(findData,updateChatlogs,_upsert,collectionName)
    _userid = findQuery(findData,projectData,collectionName)
    _userid=_userid[0]
    return _userid["userid"]


def insert_intent_entity_info(coversationidvalue,messagevalue,intentvalue,intentconfidencevalue,entityvalue,isnlp):

    data = {
        "coversationidvalue":coversationidvalue,
        "message":messagevalue,
        "intent": intentvalue,
        "intentconfidencevalue":intentconfidencevalue,
        "entity": entityvalue,
        "isnlp": isnlp
    }
    collectionName='intent_entity_data'
    
    insertQuery(data,collectionName)
    return()

def conversationHistory(conversationID):

    result=dict()
    collectionName='sequencedetails'
    _cursor=lookupQuery(conversationID,collectionName)
    return _cursor

def lookupQuery(conversationID,collectionName):
    
    collection = getDb()[collectionName]
    _cursor=list(collection.aggregate([
    {
    "$lookup":{
        "from": "userConversationTimeDetails",
        "localField": "conversationid",
        "foreignField": "conversationid",
        "as": "user_info"  
        }
    },
    {
    "$lookup":{
        "from": "messages", 
        "localField": "messageid", 
        "foreignField": "messageid",
        "as": "message"
        }
    },
    {
    "$match":{
        "$and":[{"conversationid" : conversationID}]
        }
    },
    {   
    "$project":{
        '_id':0,
        "message.message" : 1,
        "isbot" : 1,
        "seqno":1
        } 
    }
            ]))

    return _cursor