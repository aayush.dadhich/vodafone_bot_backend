import pymongo
__connection__ = {}


def initializeDB(config):
    url = config.get('url', 'mongodb://localhost:27017/')
    username = config.get('username', None)
    password = config.get('password', None)
    maxPoolSize = config.get("maxPoolSize", 10)
    idleTimeout = config.get("idleTimeout", 100)
    sslEnable = config.get("ssl_enabled", False)
    dbName=config.get('dbName')
    if not sslEnable:
        client = pymongo.MongoClient(
            url,
            maxPoolSize=maxPoolSize,
            waitQueueTimeoutMS=idleTimeout,
            username=username,
            password=password
        )
    else:
        client = pymongo.MongoClient(
            url,
            maxPoolSize=maxPoolSize,
            waitQueueTimeoutMS=idleTimeout,
            username=username,
            password=password,
            ssl=True,
            ssl_certfile='/certs/api.pem',
            ssl_keyfile='/certs/api.pem'
        )
    __connection__["mongo"] = client[dbName]


def getMongoDb():
    return __connection__["mongo"]


def getDb():
    return getMongoDb()
