import datetime
import json
import random
from PaxcelChatBotFramework import Program
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from dataAccessLayer.dbHandler import insertFeedback, conversationHistory
from dataAccessLayer.dashboardAnalytics import getUserByDate, getMsgByDate, getConvByDate, getLeadGeneratedByDate, getUnidentifiedIntent
from logger.performanceLogger import startlogging, stoplogging
from decisionEngine.actions.getFeedback import getfeedback
from VodafoneBotBackend import setup


@csrf_exempt
def processWebRequest(request):
    start = startlogging()

    request = dict(json.loads(request.body))
    p = Program()
    outputDict = p.Process(request)
    extractContext = outputDict.__json__()
    extractContext = clearContext(extractContext)
    jsonOutput = json.dumps(
        extractContext, sort_keys=True, indent=2, separators=(",", ": ")
    )
    result = HttpResponse(
        jsonOutput, content_type="application/json", charset="UTF-8")

    stoplogging(start, "Total time used by backend :")
    return result


@csrf_exempt
def feedbackByConversation(request):

    start = startlogging()
    request = dict(json.loads(request.body))
    if "conversationId" not in request["context"]:
        newConv = random.random()
        request["context"]["conversationId"] = str(newConv)

    uuid = request["context"]["conversationId"]
    feedback = request["input"]["text"]

    chatlogsId = insertFeedback(uuid, str(start), feedback)
    request["context"]["logsId"] = chatlogsId[0]
    result = HttpResponse(
        request, content_type="application/json", charset="UTF-8")
    
    stoplogging(start, "Total time used by backend :")  
    return result


@csrf_exempt
def getChatHistory(request):
    request = dict(json.loads(request.body))
    conversationID = request["convId"]
    data = conversationHistory(conversationID)
    result = HttpResponse(json.dumps(
        data), content_type="application/json", charset="UTF-8")
    return result


@csrf_exempt
def getFeedback(request):
    request = dict(json.loads(request.body))
    feedbackinput = request["feedback"]
    if 'email' in request:
        userinput = request['email']
    else:
        userinput = setup.config['defaultUserVariables']['userinput']

    data = getfeedback(feedbackinput, userinput)
    result = HttpResponse(json.dumps(
        data), content_type="application/json", charset="UTF-8")
    return result


def clearContext(extractContext):

    if "scholarship" in extractContext['context']:
        del extractContext['context']['scholarship']

    if "userScholarships" in extractContext['context']:
        del extractContext['context']['userScholarships']

    if "allScholarships" in extractContext['context']:
        del extractContext['context']['allScholarships']

    if "querylist" in extractContext['context']:
        del extractContext['context']['querylist']

    if 'exploreMore' in extractContext['context']:
        del extractContext['context']['exploreMore']

    if 'scholarshiptitles' in extractContext['context']:
        del extractContext['context']['scholarshiptitles']

    if 'Classnlp' in extractContext['context']:
        del extractContext['context']['Classnlp']

    if 'Gendernlp' in extractContext['context']:
        del extractContext['context']['Gendernlp']

    if 'Categorynlp' in extractContext['context']:
        del extractContext['context']['Categorynlp']

    if 'Preferencenlp' in extractContext['context']:
        del extractContext['context']['Preferencenlp']

    if 'Age' in extractContext['context']:
        del extractContext['context']['Age']

    if 'Subject' in extractContext['context']:
        del extractContext['context']['Subject']

    if 'State' in extractContext['context']:
        del extractContext['context']['State']

    if 'AnnualFamilyIncome' in extractContext['context']:
        del extractContext['context']['AnnualFamilyIncome']

    if 'Religion' in extractContext['context']:
        del extractContext['context']['Religion']

    return extractContext


@csrf_exempt
def totalUserByDayRequest(request):
    req = json.loads(request.body)

    StartDate = req['startDate']
    EndDate = req['endDate']
    data = getUserByDate(StartDate, EndDate)
    jsonOutput = json.dumps(
        data, sort_keys=True, indent=2, separators=(",", ": ")
    )
    result = HttpResponse(
        jsonOutput, content_type="application/json", charset="UTF-8")
    return result


@csrf_exempt
def totalMsgByDayRequest(request):
    req = json.loads(request.body)
    StartDate = req['startDate']
    EndDate = req['endDate']
    data = getMsgByDate(StartDate, EndDate)
    jsonOutput = json.dumps(
        data, sort_keys=True, indent=2, separators=(",", ": ")
    )
    result = HttpResponse(
        jsonOutput, content_type="application/json", charset="UTF-8")
    return result


@csrf_exempt
def totalConvByDayRequest(request):
    req = json.loads(request.body)
    StartDate = req['startDate']
    EndDate = req['endDate']
    data = getConvByDate(StartDate, EndDate)
    jsonOutput = json.dumps(
        data, sort_keys=True, indent=2, separators=(",", ": ")
    )
    result = HttpResponse(
        jsonOutput, content_type="application/json", charset="UTF-8")
    return result


@csrf_exempt
def getLeadGeneratedByDateRequest(request):
    req = json.loads(request.body)
    StartDate = req['startDate']
    EndDate = req['endDate']
    data = getLeadGeneratedByDate(StartDate, EndDate)
    jsonOutput = json.dumps(
        data, sort_keys=True, indent=2, separators=(",", ": ")
    )
    result = HttpResponse(
        jsonOutput, content_type="application/json", charset="UTF-8")
    return result


@csrf_exempt
def getUnidentifiedIntentRequest(request):
    req = json.loads(request.body)
    StartDate = req['startDate']
    EndDate = req['endDate']
    data = getUnidentifiedIntent(StartDate, EndDate)
    jsonOutput = json.dumps(
        data, sort_keys=True, indent=2, separators=(",", ": ")
    )
    result = HttpResponse(
        jsonOutput, content_type="application/json", charset="UTF-8")
    return result
