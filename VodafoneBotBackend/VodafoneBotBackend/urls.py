"""VodafoneBotBackend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from startApp.webstart import processWebRequest,feedbackByConversation, getChatHistory, totalUserByDayRequest,getFeedback, totalMsgByDayRequest
from startApp.webstart import totalConvByDayRequest, getLeadGeneratedByDateRequest, getUnidentifiedIntentRequest
from .setup import setup

setup()
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r"^chatbotEngine", processWebRequest, name="processWebRequest"),
    url(r"^feedback", feedbackByConversation, name="feedbackByConversation"),
    url(r"^getChatHistory", getChatHistory, name="getChatHistory"),
    url(r"^getFeedback", getFeedback, name="getFeedback"),
    url(r"^totalUserByDayRequest", totalUserByDayRequest, name="totalUserByDayRequest"),
    url(r"^totalMsgByDayRequest", totalMsgByDayRequest, name="totalMsgByDayRequest"),
    url(r"^totalConvByDayRequest", totalConvByDayRequest, name="totalConvByDayRequest"),
    url(r"^getLeadGeneratedByDateRequest", getLeadGeneratedByDateRequest, name="getLeadGeneratedByDateRequest"),
    url(r"^getUnidentifiedIntentRequest", getUnidentifiedIntentRequest, name="getUnidentifiedIntentRequest")
    ]
