from PaxcelChatBotFramework import Program, ComponentManager
from inputProcessor import WebInputProcessor
from outputProcessor import WebOutputProcessor
from bots import TemplateBot, NLPBot
from decisionEngine import DecisionEngine
from dataAccessLayer import dbConnectionPooling
import os,json
path_json=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
with open(path_json+'\\VodafoneBotBackend\\config.json') as config:
    config=json.load(config)


def setup():
    manager = ComponentManager()
    manager.registerInputProcessor(WebInputProcessor)
    manager.registerBot(TemplateBot)
    NLPBot.Initialize(config['watsonAssistant'])
    dbConnectionPooling.initializeDB(config['dbConfig'])
    manager.registerBot(NLPBot)
    DecisionEngine.Initialize()
    manager.registerRuleEngine(DecisionEngine)
    manager.registerOutputProcessor(WebOutputProcessor)
    Program.initialize(manager)
