import json
from businessLogicLayer import DataMapping
from .getByIdAndLanguageResponse import GetByIdAndLanguageResponse

class GetByIdAndLanguageAllScholarships(GetByIdAndLanguageResponse):

    def formatSuccessResponse(self,response,context):

        apidata = response.json()
        apidata_response_array=self.getapidataResponseArray(apidata,context)
        dataarray=self.getDatAarray(apidata,context)
        data=self.getFinalData(dataarray,context,apidata_response_array)
        return data

    def getFinalData(self,dataarray,context,apidata_response_array):
        
        self.setDataLength(apidata_response_array,context)
        data = {
            '$scholarship': dataarray,
            "$datalen": str(context["$datalen"]),
            '$scholarshiptitles': apidata_response_array
        }
        return data

    def setDataLength(self,apidata_response_array,context):

        length = len(apidata_response_array)
        if length == 0:
            context["$datalen"] = True
        else:
            context["$datalen"] = False

    def getapidataResponseArray(self,apidata,context):

        language=self.paramshelper.getLanguage(context)
        apidata_response_array = []
        if "data" in apidata:
            for countrandom in range(0, len(apidata["data"])):
                if (apidata['data'][countrandom]['active'] == True) or (apidata['data'][countrandom]['publish_later'] == True):
                    if language in apidata["data"][countrandom]["content_by_language"]:
                        if "title" in apidata["data"][countrandom]["content_by_language"][language]:
                            apidata_response = apidata["data"][countrandom]["content_by_language"][language]["title"]
                            apidata_response_array.append(apidata_response)
        return apidata_response_array

    def getDatAarray(self,apidata,context):

        language=self.paramshelper.getLanguage(context)
        dataarray = []
        if ('data' in apidata) and (len(apidata['data']) != 0):
            for count in range(0, len(apidata['data'])):
                if apidata['data'][count]['language'] == language:
                    if ((apidata['data'][count]['active'] == True) or (apidata['data'][count]['publish_later'] == True)) and "apply_online_link" in apidata['data'][count]:
                        
                        sch_list=DataMapping.getSchList(apidata,count,language)
                        dataarray.append(sch_list)
        return dataarray

    def getRequestPayLoad(self,context):

        params = self.paramshelper.getParamsByLanguage(context)
        return params