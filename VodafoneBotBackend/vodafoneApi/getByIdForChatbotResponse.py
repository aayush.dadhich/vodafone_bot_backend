import json
from . import ApiHandler
class GetByIdForChatbotResponse(ApiHandler):
    
    def __init__(self,user_scholarships):
        self.apiMethod="post"
        self.user_scholarships=user_scholarships

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + '/student/getByIdForChatbot'
        return url

    def getRequestPayLoad(self,context):

        email = context["$email"][0]
        params={"email": email}
        return params
    
    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        getUserScholarships=self.getUserScholarships(apidata,self.user_scholarships)
        return getUserScholarships
    
    def getUserScholarships(self,apidata,user_scholarships):

        name = apidata["data"]["personalDetails"]["FirstName"] + \
            " " + apidata["data"]["personalDetails"]["LastName"]
        final_scholarships_applied = {
            'userid': apidata["data"]["_id"],
            'name': name,
            'scholarships': user_scholarships
        }
        return final_scholarships_applied