import json
from . import ApiHandler
class StudentLoginProfileCompletionResponse(ApiHandler):

    def __init__(self):
        self.apiMethod="post"

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + '/student/login'
        return url

    def getRequestPayLoad(self,context):

        email = context["$email"][0],
        password = config["defaultApiValues"]["password"],
        params = {
        "email": email,
        "password": password
        }
        return params
    
    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        data = {

            "$profile": apidata["data"],
            "$isProfileCompleted": apidata["data"]["isProfileCompleted"]
        }
        return data