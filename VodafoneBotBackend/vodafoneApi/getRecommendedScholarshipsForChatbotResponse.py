from .getSuccesScholarshipResponseForWatson import GetSuccessScholarshipResponseForWatson
class GetRecommendedScholarshipsForChatbotResponse(GetSuccessScholarshipResponseForWatson):

    def __init__(self):
        self.apiMethod="post"

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + '/student/getRecommendedScholarshipsForChatbot'
        return url

    def getRequestPayLoad(self,context):

        params = self.paramshelper.getParamsByEmail(context)
        return params
    
    def getFinalData(self,dataarray,context):

        data = {

            "$scholarship": dataarray,
            "$datalength": str(context["$datalength"]),
        }
        return data

