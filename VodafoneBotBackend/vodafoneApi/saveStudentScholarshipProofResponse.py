import json
from . import ApiHandler
class SaveStudentScholarshipProofResponse(ApiHandler):

    def __init__(self):
        self.apiMethod="post"

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + '/student/saveStudentScholarshipProof'
        return url

    def getRequestPayLoad(self,context):

        userid = context["$uploadparam"][0]["userid"]
        Title = context["$uploadparam"][0]["Title"]
        Year = context["$uploadparam"][0]["Year"]
        Description = context["$uploadparam"][0]["Description"]
        ScholarshipId = context["$uploadparam"][0]["ScholarshipId"]
        DocUrl = context["$uploadparam"][0]["DocUrl"]
        Name = context["$uploadparam"][0]["Name"]

        params = {
            "file": {
                "filename": DocUrl,
                "originalname": Name
            },
            "data": {
                "_id": userid,
                "Title": Title,
                "Year": Year,
                "Description": Description,
                "ScholarshipId": ScholarshipId,
                "DocUrl": DocUrl,
                "Source":"Chatbot"
            }
        }
        return params
    
    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        data = {

            "$uploadreceipt": apidata["data"]

        }
        return data