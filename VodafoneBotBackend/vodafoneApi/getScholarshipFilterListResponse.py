import json
from . import ApiHandler
class GetScholarshipFilterListResponse(ApiHandler):

    def __init__(self):
        self.apiMethod="get"

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + '/utility/getScholarshipFilterList'
        return url

    def getRequestPayLoad(self,context):

        params={}
        return params
    
    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        data = {

            "$exploreMore": apidata["data"]

        }
        return data