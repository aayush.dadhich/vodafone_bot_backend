import json
from . import ApiHandler
class GetReceivedAndFavouriteScholarshipsByEmailResponse(ApiHandler):
    
    def __init__(self):
        self.apiMethod="post"
        
    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + '/student/getReceivedAndFavouriteScholarshipsByEmail'
        return url

    def getRequestPayLoad(self,context):

        email = context["$email"][0]
        params={"email": email}
        return params
    
    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        user_scholarships=self.getUserScholarships(apidata,context)
        return user_scholarships
    
    def getUserScholarships(self,apidata,context):

        language=self.paramshelper.getLanguage(context)
        user_scholarships = []
        if "data" in apidata:
            for count in range(0, len(apidata["data"])):
                if language in apidata["data"][count]["content_by_language"]:
                    user_applied_fav = apidata["data"][count]["content_by_language"][language]
                    user_applied_fav.update({"Sid": apidata["data"][count]["_id"]})
                    user_scholarships.append(user_applied_fav)
        return user_scholarships