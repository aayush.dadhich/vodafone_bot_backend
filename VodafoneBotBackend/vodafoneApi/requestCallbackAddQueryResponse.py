import json
from . import ApiHandler
class RequestCallbackAddQueryResponse(ApiHandler):

    def __init__(self):
        self.apiMethod="post"     

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + '/query/addQuery'
        return url

    #override this function
    def getRequestPayLoad(self,context):
        pass
    
    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        data = {

            "$requestcallback": apidata["data"],
            "$ticketid": apidata["data"]["ticket"],

        }

        return data