import json
from .getByIdAndLanguageResponse import GetByIdAndLanguageResponse
from businessLogicLayer import DataMapping

class GetScholarshipByIdAndLanguage(GetByIdAndLanguageResponse):

    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        dataarray=self.getDataArray(apidata,context)
        self.setDataLength(dataarray,context)
        data=self.getFinalData(dataarray,context)
        return data

    def getDataArray(self,apidata,context):

        language=self.paramshelper.getLanguage(context)
        dataarray = []
        if ('data' in apidata) and (len(apidata['data']) != 0):
            for count in range(0, len(apidata['data'])):
                if language in apidata['data'][count]['content_by_language']:
                    if ((apidata['data'][count]['active'] == True) or (
                            apidata['data'][count]['publish_later'] == True)) and "apply_online_link" in \
                            apidata['data'][count]:
                        
                        sch_list=DataMapping.getSchList(apidata,count,language)
                        dataarray.append(sch_list)
        return dataarray

    def setDataLength(self,dataarray,context):

        length = len(dataarray)
        if length == 0:
            context["$datalength"] = True
        else:
            context["$datalength"] = False

    def getFinalData(self,dataarray,context):

        data = {

            "$scholarship": dataarray,
            "$datalength": str(context["$datalength"]),
        }

        return data
