import json
from . import ApiHandler
class GetScholarshipByIdsResponse(ApiHandler):

    def __init__(self):
        self.apiMethod="post"

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + "/scholarship/getScholarshipByIds"
        return url

    def getRequestPayLoad(self,context):

        id = context["$ScholarshipsIdList"][0]
        params = {
            "id": id,
        }
        return params
    
    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        data = {

            "$appliedScholarshipsList": apidata["data"]

        }
        return data