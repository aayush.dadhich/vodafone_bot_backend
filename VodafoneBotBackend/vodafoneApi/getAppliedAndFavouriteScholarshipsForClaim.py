from .getReceivedAndFavouriteScholarshipsByEmailResponse import GetReceivedAndFavouriteScholarshipsByEmailResponse
from .getByIdAndLanguageResponse import GetByIdAndLanguageResponse
from .getByIdForChatbotResponse import GetByIdForChatbotResponse

class GetAppliedAndFavouriteScholarshipsForClaim():

    def execute(self,context):
        
        api=GetReceivedAndFavouriteScholarshipsByEmailResponse()
        user_scholarships=api.execute(context)

        api=GetByIdForChatbotResponse(user_scholarships)
        final_scholarships_applied_favourite=api.execute(context)

        api=GetByIdAndLanguageResponse()
        apidata_response_array=api.execute(context)
    
        api=GetByIdForChatbotResponse(apidata_response_array)
        final_allscholarship=api.execute(context)

        responselength=self.getResponseLength(user_scholarships,apidata_response_array)
        response=self.getFinalData(responselength,final_scholarships_applied_favourite,final_allscholarship)

        return response

    def getFinalData(self,responselength,final_scholarships_applied_favourite,final_allscholarship):

        data = {

            "$userScholarships": final_scholarships_applied_favourite,
            "$allScholarships": final_allscholarship,
            "$responselength": str(responselength),
        }
        return data    

    def getResponseLength(self,user_scholarships,apidata_response_array):

        if len(user_scholarships) == 0 and len(apidata_response_array) == 0:
            responselength = True
        else:
            responselength = False
        return responselength
 