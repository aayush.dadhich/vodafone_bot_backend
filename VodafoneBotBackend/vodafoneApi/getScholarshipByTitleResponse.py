import json
from . import ApiHandler
from businessLogicLayer import DataMapping
class GetScholarshipByTitleResponse(ApiHandler):

    def __init__(self):
        self.apiMethod="post"

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + '/scholarship/getScholarshipByTitle'
        return url

    def getRequestPayLoad(self,context):

        params= {"title":context["$title"][0]}
        return params
    
    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        dataarray=self.getDataArray(apidata,context)
        self.setDataLength(dataarray,context)
        data=self.getFinalData(dataarray,context)
        return data

    def getDataArray(self,apidata,context):

        language=self.paramshelper.getLanguage(context)
        dataarray = []
        if ('data' in apidata) and  (len(apidata['data']) != 0):
            for count in range(0, len(apidata['data'])):
                    if language in apidata['data'][count]['content_by_language'] and "apply_online_link" in apidata['data'][count] and "slug" in apidata['data'][count] and "media_url" in \
                            apidata['data'][count] and "_id" in apidata['data'][count]:
                            
                            sch_list=DataMapping.getSchList(apidata,count,language)
                            dataarray.append(sch_list)
        return dataarray

    def setDataLength(self,dataarray,context):

        length = len(dataarray)
        if length == 0:
            context["$datalength"] = True

        else:
            context["$datalength"] = False

    def getFinalData(self,dataarray,context):

        data = {
            "$scholarship": dataarray,
            "$datalength": str(context["$datalength"]),
        }
        return data
