import json
from . import ApiHandler
class GetEncriptResponse(ApiHandler):

    def __init__(self):
        self.apiMethod="post"
        
    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + "/responce/encript"
        return url

    def getRequestPayLoad(self,context):

        _url=self.getUrl()
        email = context["$email"][0]
        name = context["$name"][0]
        keys = context["$emailkey"][0]
        key = keys
        myURL = _url["siteURL"] + "/verifyUser/" + key
        subject = 'Verification Email From Learning With Vodafone Idea'
        params={
            "username":name,
            "link":myURL,
            "subject":subject,
            "email":email,
            "actionType":"VerifyUserEmail"
        }
        return params
    
    def formatSuccessResponse(self, response, context):
        
        response = response.json()
        context["~EncriptApiResp"]=response
        return response