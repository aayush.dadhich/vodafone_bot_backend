import json
from . import ApiHandler
class GetMasterLanguageResponse(ApiHandler):

    def __init__(self):
        self.apiMethod="get"

    def getRelativeUrl(self):

        language=self.paramshelper.getDefaultLanguage()
        _url=self.getUrl()
        url = _url["mainURL"] + '/master/getMaster?language=' + language
        return url

    def getRequestPayLoad(self,context):

        params={}
        return params
    
    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        data = {

            "$querylist": apidata["data"][0]["supportQuery"]

        }
        return data