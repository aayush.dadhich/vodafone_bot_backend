import json
from .getFavouriteScholarshipsResponse import GetFavouriteScholarshipsResponse
import logging
LOGGER = logging.getLogger(__name__)
    
class GetFavouriteScholarshipsListNlpFlow(GetFavouriteScholarshipsResponse):

    def execute(self,context):

        params=self.getRequestPayLoad(context)
        url=self.getRelativeUrl()
        header=self.getRequestHeader()

        LOGGER.info("API Request %s", {"url" :url, "payload": params, "header": header})

        response =self.apiCall(url, params, header,self.apiMethod)
        LOGGER.info("API Response %s", {"responseStatus" : response.status_code,"response":response})

        formattedResponse =self.formatSuccessResponse(response,context)
        return formattedResponse

    def formatSuccessResponse(self,response,context):

        apidata = response.json()
        dataarray=self.getDataArray(apidata)
        data=self.getFinalData(dataarray,context)
        return data

    def getDataArray(self,apidata):

        dataarray = []
        if 'data' in apidata and "FavouriteScholarships" in apidata["data"][0]["scholarshipInterest"]:
            favouritescholarships = apidata["data"][0]["scholarshipInterest"]["FavouriteScholarships"]
            for count in range(0, len(favouritescholarships)):
                if language in favouritescholarships[count]['content_by_language']:
                    if ("apply_online_link" in favouritescholarships[count]):
                        sch_list = {
                            "id": favouritescholarships[count]['content_by_language'][language]["_id"],
                            "publish_later": favouritescholarships[count]["publish_later"],
                            "valid_till": favouritescholarships[count]["valid_till"],
                            "apply_online_link": favouritescholarships[count]["apply_online_link"],
                            "media_url": favouritescholarships[count]["media_url"],
                            "listitem": favouritescholarships[count]['content_by_language'][language],
                            "slug": favouritescholarships[count]["slug"]

                        }
                        dataarray.append(sch_list)
        return dataarray

    def getFinalData(self,dataarray,context):

        self.setFavLength(dataarray,context)
        data = {

            "$scholarship": dataarray,
            "$favlength": str(context["$favlength"])
        }
        return data

    def setFavLength(self,dataarray,context):

        length = len(dataarray)
        if length == 0:
            context["$favlength"] = True
        else:
            context["$favlength"] = False