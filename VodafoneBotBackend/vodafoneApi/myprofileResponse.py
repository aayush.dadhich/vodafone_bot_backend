from . import ApiHandler
class MyprofileResponse(ApiHandler):

    def execute(self,context):
        url=self.getRelativeUrl()
        response=self.formatSuccessResponse(url)
        return response

    def getRequestPayLoad(self, context):
        pass

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["siteURL"] + '/'+'myprofile'
        return url

    def formatSuccessResponse(self,url):

        data = {
            "$Profileurl": url
        }
        return data