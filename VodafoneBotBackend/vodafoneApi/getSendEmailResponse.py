import json
from . import ApiHandler
class GetSendEmailResponse(ApiHandler):

    def __init__(self):
        self.apiMethod="post"
        
    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + "/email/sendEmail"
        return url

    def getRequestPayLoad(self,context):

        context=context["~EncriptApiResp"]

        params = {
            "username":context["data"]["username"],
            "link":context["data"]["link"],
            "subject":context["data"]["subject"],
            "email": context["data"]["email"],
            "html": "Chatbot",
            "actionType":context["data"]["actionType"]
        }
        return params
    
    def formatSuccessResponse(self, response, context):

        response = response.json()
        return response