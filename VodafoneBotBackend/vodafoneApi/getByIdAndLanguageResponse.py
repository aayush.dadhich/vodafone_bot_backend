import json
from . import ApiHandler
class GetByIdAndLanguageResponse(ApiHandler):
    
    def __init__(self):
        self.apiMethod="post"

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + '/scholarship/getByIdAndLanguage'
        return url

    def getRequestPayLoad(self,context):

        params=self.paramshelper.getParamsByLanguage(context)
        return params
    
    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        apidata_response_array=self.getApidataResponseArray(apidata,context)
        return apidata_response_array
    
    def getApidataResponseArray(self,apidata,context):

        language=self.paramshelper.getLanguage(context)
        apidata_response_array = []
        if "data" in apidata:
            for countrandom in range(0, len(apidata["data"])):
                if ((apidata['data'][countrandom]['active'] == True) or (
                        apidata['data'][countrandom]['publish_later'] == True)):
                        if "title" in apidata["data"][countrandom]["content_by_language"][language]:
                            apidata_response = apidata["data"][countrandom]["content_by_language"][language]
                            apidata_response.update({"Sid": apidata["data"][countrandom]["_id"]})
                            apidata_response_array.append(apidata_response)
        return apidata_response_array
