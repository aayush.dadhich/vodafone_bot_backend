import json
from .requestCallbackAddQueryResponse import RequestCallbackAddQueryResponse

class SubmitUserProvidedQuery(RequestCallbackAddQueryResponse):

    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        data = {

            "$submitquery": apidata["data"],
            "$ticketid": apidata["data"]["ticket"],

        }
        return data

    def getRequestPayLoad(self,context):

        name = context["$name"][0]
        email = context["$email"][0]
        phoneno = context["$phoneno"][0]
        query = context["$query"][0]
        params = {
            "name": name,
            "email": email,
            "mobile": phoneno,
            "query": query,
            "source": "Chatbot",
            "host":"Query",
            "communicationMode": "Email",
            "communicationMedium": email
        }
        return params