import json
from . import ApiHandler
class SaveUserActivityResponse(ApiHandler):

    def __init__(self,_input, arg):
        self.apiMethod="post"
        self._input=_input
        self.arg=arg

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + "/activity/saveUserActivity"
        return url

    def getRequestPayLoad(self,context):

        entityid = context["$entityId"][0]
        sessionid = context["$sessionId"][0]
        userId = context["$userId"][0]
        language = context["$Language"][0]
        setdata = []
        for entity in entityid:

            params = {

                "eventType": "OK_CHATBOT",
                "entityName": "ok_chatbot",
                "entityId": entity,
                "sessionId": sessionid,
                "language": language,
                "userId": userId

            }
        return params
    
    def formatSuccessResponse(self, response, context):

        apidata = response.json()
        dataresult = {
                "$scholarshipsSaved": apidata["data"],
            }
        setdata.append(dataresult)
        data = {'$dataScholarshipsSaved': setdata}

        return data
  