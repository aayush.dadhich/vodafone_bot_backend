from abc import ABC, abstractmethod
import logging
import json
import requests
from businessLogicLayer import ParamsHelper
from VodafoneBotBackend import settings
config=settings.config
LOGGER = logging.getLogger(__name__)

errorMsg = {
    "400" : "Your request is invalid.",
    "401" : "The requested page needs a username and a password",
    "403" : "We understand your request, but access is forbidden to the requested page.",
    "404" : "The server can not find the requested page. You are requesting an invalid URI",
    "500" : "The request was not completed. We'll be notified and we'll look into it.",
    "502" : "The request was not completed. The server received an invalid response from the upstream server",
    "DEFAULT": "Sorry something went wrong"
}

class ApiHandler(ABC):

    @abstractmethod
    def getRelativeUrl(self) -> str:
        """Return the relative url for the api call"""

    @abstractmethod
    def getRequestPayLoad(self, context)-> dict:
        """Return the request params"""

    @abstractmethod
    def formatSuccessResponse(self, response, context) -> dict:
        """Format the success api response"""

    @staticmethod
    def getUrl():

        _Url={"mainURL":config["vodafoneUrls"]["mainURL"],
        "siteURL":config["vodafoneUrls"]["siteURL"]}
        return _Url

    @classmethod
    def getRequestHeader(cls)-> dict:

        return {
            "content-Type": "application/json"
        }

    def formatErrorResponse(self,response):

        statusCode = str(response.status_code)

        errorResponse = {
                        "error" : {
                            "status" : statusCode,
                            "message" : errorMsg.get(statusCode, errorMsg['DEFAULT'])},
                        }
        return errorResponse

    def apiCall(self,url, data, header,apiMethod):

        response = {}
        if apiMethod == "get":
            response = requests.get(url, params=data, headers=header)
        elif apiMethod == "post":
            response = requests.post(url, data=json.dumps(data), headers=header)
        return response

    def handleResponse(self,response,context):

        if response.status_code == 200:
            return self.formatSuccessResponse(response,context)
        return self.formatErrorResponse(response)

    def execute(self,context):

        self.paramshelper=ParamsHelper()
        params=self.getRequestPayLoad(context)
        url=self.getRelativeUrl()
        header=self.getRequestHeader()

        LOGGER.info("API Request %s", {"url" :url, "payload": params, "header": header})

        response =self.apiCall(url, params, header,self.apiMethod)
        LOGGER.info("API Response %s", {"responseStatus" : response.status_code,"response":response})

        formattedResponse =self.handleResponse(response,context)
        return formattedResponse
