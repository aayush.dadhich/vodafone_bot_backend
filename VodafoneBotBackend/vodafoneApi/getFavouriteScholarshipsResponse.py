import json
from . import ApiHandler
import requests
class GetFavouriteScholarshipsResponse(ApiHandler):

    def __init__(self):
        self.apiMethod="get"

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + '/student/getFavouriteScholarships' + '/' + self.email
        return url

    def getRequestPayLoad(self,context):

        self.email=self.paramshelper.getEmail(context)
        params={}
        return params
    
    def apiCall(self,url, data, header,apiMethod):

        response = requests.get(url, headers=header)
        return response

    #override this function if required
    def formatSuccessResponse(self, response, context):

        context["~scholarshipIdList"]=response
        return response