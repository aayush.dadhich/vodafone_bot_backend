from .requestCallbackAddQueryResponse import RequestCallbackAddQueryResponse
from .requestCallbackUpdateQueryResponse import RequestCallbackUpdateQueryResponse
from businessLogicLayer import GetRequestCallbackByEmailParams

class RequestCallbackByEmail():

    def execute(self,context):

        if '$submitquery' in context and 'ticket' in context['$submitquery']:

            api=UpdateQueryResponse()
            response=api.execute(context)

        else:

            api=AddQueryResponse()
            response=api.execute(context)
        
        self.rmvBusinessContext(context)
        return response

    def rmvBusinessContext(self,context):

        if '$ticketid' in context:
            del context['$ticketid']
        if '$submitquery' in context:
            del context["$submitquery"]

class AddQueryResponse(RequestCallbackAddQueryResponse):

    def getRequestPayLoad(self,context):

        payload=GetRequestCallbackByEmailParams()
        params="addQuery"
        params=payload.getParams(context,params)
        return params
    
class UpdateQueryResponse(RequestCallbackUpdateQueryResponse):

    def getRequestPayLoad(self,context):

        payload=GetRequestCallbackByEmailParams()
        params="updateQuery"
        params=payload.getParams(context,params)
        return params