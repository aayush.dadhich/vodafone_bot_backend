from .getSuccesScholarshipResponseForWatson import GetSuccessScholarshipResponseForWatson
from businessLogicLayer import GetScholarshipParams
class GetScholarshipByFilterResponse(GetSuccessScholarshipResponseForWatson):

    def __init__(self):
        self.apiMethod="post"

    def getRelativeUrl(self):

        _url=self.getUrl()
        url = _url["mainURL"] + '/scholarship/getScholarshipByFilter'
        return url

    def getRequestPayLoad(self,context):

        obj=GetScholarshipParams()
        params=obj.getParamsBySelectedCriteria(context)
        return params

    def getFinalData(self,dataarray,context):

        data = {
            "$scholarship": dataarray,
            "$status_scholarships": self.apidata["status"],
            "$datalength": str(context["$datalength"])
        }

        return data