import json
from businessLogicLayer import DataMapping
from .getScholarshipByIdsResponse import GetScholarshipByIdsResponse

class GetClaimedScholarshipsForNlp(GetScholarshipByIdsResponse):

    def getRequestPayLoad(self,context):

        params={"id": context.get("~scholarshipIdList")}
        return params

    def formatSuccessResponse(self,response,context):

        apidata = response.json()
        scholarshipIdList=self.getScholarshipIdList(apidata)
        
        dataarray=[]
        if len(scholarshipIdList) !=0:
            apidata2 = response.json()
            dataarray=self.getDataArray(apidata2,dataarray)

        data=self.getFinalData(context,dataarray)
        return data

    def getScholarshipIdList(self,apidata):

        scholarshipIdList=[]
        if 'data' in apidata and 'scholarshipHistory' in apidata["data"][0] and 'ReceivedScholarships' in apidata["data"][0]["scholarshipHistory"]:
            claimedScholarships=apidata["data"][0]["scholarshipHistory"]['ReceivedScholarships']
            for count in range(0,len(claimedScholarships)):
                if 'ScholarshipId' in claimedScholarships[count] and claimedScholarships[count]['ScholarshipId'] != None:
                    scholarshipIdList.append(claimedScholarships[count]['ScholarshipId'])
        return scholarshipIdList

    def getDataArray(self,apidata,dataarray):

        language=self.paramshelper.getLanguage(context)
        if 'data' in apidata and len(apidata['data'] !=0):
            for newcount in range(0,len(apidata['data'])):
                if language in apidata['data'][newcount]['content_by_language'] and "apply_online_link" in apidata['data'][newcount] \
                        and "slug" in apidata['data'][newcount] and "media_url" in apidata['data'][newcount] \
                        and "_id" in apidata['data'][newcount]:

                        sch_list=DataMapping.getSchList(apidata,newcount,language)
                        dataarray.append(sch_list)    
        return dataarray

    def getFinalData(self,context,dataarray):

        self.setClaimLength(dataarray,context)
        data = {
            "$scholarship": dataarray,
            "$claimlength": str(context["$claimlength"])
        }
        return data

    def setClaimLength(self,dataarray,context):

        length = len(dataarray)
        if length == 0:
            context["$claimlength"] = True
        else:
            context["$claimlength"] = False