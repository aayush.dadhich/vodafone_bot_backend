from .weboutputProcessor import WebOutputProcessor

__all__ = [
    "WebOutputProcessor",
]