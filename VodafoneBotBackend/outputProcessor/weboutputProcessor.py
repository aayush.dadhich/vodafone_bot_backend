from PaxcelChatBotFramework import OutputProcessor
from PaxcelChatBotFramework.Models import Response
from dataAccessLayer.dbHandler import insertChatlogs, updateChatlogs
from logger.performanceLogger import startlogging, stoplogging
from VodafoneBotBackend import setup
import random
import json
import datetime
import logging
from PaxcelChatBotFramework import Program
logger = logging.getLogger(__name__)
class WebOutputProcessor(OutputProcessor):

    @staticmethod
    def CanHandle(request) -> bool:
        
        return request.AdditionalData["source"] == "web"

    def Process(self, request):

        try:
            start = startlogging()
            context = request.Context
            output = request.Output
            additionalData = request.AdditionalData
            response = Response()
            response.Data = {"Output":output,"AdditionalData":additionalData}
            response.Context = context


            jsonOutput = json.dumps(response.__json__(), sort_keys=True, indent=2, separators=(',', ': '))
            OutputJson = json.loads(jsonOutput)
            
            botMessage = self.formatLogMessage(OutputJson)
            conversationId=self.getconversationId(OutputJson)
            userId=self.getuserId(OutputJson)


            self.addConversationLog(botMessage,conversationId,userId)
            
            stoplogging(start, "Total time used by OutputProcessor :")
        except:
            logger.info( "User Query: {0}".format("Exception occurred in output processor"))
            raise
        return response

    def formatLogMessage(self, OutputJson):

        botMessage = []
        for data in OutputJson["data"]:
            if "option" in data["type"]:
                botMessage.append(data["data"]["text"])
            elif "text" in data["type"]:
                botMessage.append(data["message"])
            else:
                botMessage.append("Form is rendered")
        return botMessage

    def getconversationId(self,OutputJson):
 
        conversationId = OutputJson["context"]["conversationId"]
        return(conversationId)

    def getuserId(self,OutputJson):

        userId = setup.config['defaultUserVariables']['userId']
        return(userId)

    def addConversationLog(self,botMessage,conversationId,userId):

        timeStart = datetime.datetime.now()
        for userMessage in botMessage:
            insertChatlogs(userMessage, conversationId, str(userId), str(timeStart), True)
            updateChatlogs(conversationId, str(userId))
