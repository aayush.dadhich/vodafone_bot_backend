from PaxcelChatBotFramework import Bot
from PaxcelChatBotFramework.Models import BotOutput, Intent, Entity
import json
from bots.botQuery import insertIntentEntityConversationalBot
class TemplateBot(Bot):
    def getName(self):
        return "Tempalate Bot"
    Name = property(getName)

    @staticmethod
    def CanHandle(request):
        return request.Type == "template"
    
    def Process(self, request) -> BotOutput:
        
        dataReq = request.AdditionalData
        output = BotOutput()
        input = request.Input
       
        if input.Type == "form":
            data = input.Data
            for item in data:
                value = data[item]
                if item == "#Name":
                    intent = Intent()
                    intent.Name = value[1:]
                    intent.Confidence = 1.0
                    output.Intent  = intent

                elif item[0] == "@":
                    entity = Entity()
                    entity.Name = item[1:]
                    entity.Value = value
                    entity.Confidence = 1.0
                    output.Entities.append(entity)
        if intent.Confidence:
            confidence=int(intent.Confidence)
        else:
            confidence=0
        isnlp= False
        resultQuery=insertIntentEntityConversationalBot(dataReq,confidence,isnlp)
        output.Context = request.Context
        output.AdditionalData= request.AdditionalData

        return output

            
