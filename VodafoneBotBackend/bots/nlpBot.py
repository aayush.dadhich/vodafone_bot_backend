from PaxcelChatBotFramework import Bot
from .watsonAssistant import Assistant
from NLPEngineCore import EngineInterface
from PaxcelChatBotFramework.Models import BotOutput, Intent, Entity
import logging
import json
import datetime
from dataAccessLayer.dbHandler import insert_intent_entity_info
from bots.botQuery import insertIntentEntity
logger = logging.getLogger(__name__)
class NLPBot(Bot):
    
    __assistant__ = None
    init = False
    def getName(self):
        return "NLP Bot"
    Name = property(getName)
    @staticmethod
    def Initialize(config):
        NLPBot.__assistant__ = Assistant(config)
        EngineInterface.Initialize({
            "assistant": NLPBot.__assistant__.Message,
            "getDestinationBot": NLPBot.GetDestinationBot
        })
        NLPBot.init = True

    @staticmethod
    def CanHandle( request):
        logger.info(
            "User Query: {0}".format('type message received')

        )
        return request.Type == "message"
    
    @staticmethod
    def GetDestinationBot(context):
        try:
            logger.info(
            "User Query: {0}".format('now we Get DestinationBot')
            )
            destination_bot = "AGENT"
            __BotWorkspaceIDs__ = {}
            if "workspaceIds" in context:
                __BotWorkspaceIDs__ = context["workspaceIds"]
            if context != None and "destination_bot" in context and context["destination_bot"] != None:
                destination_bot = context["destination_bot"].upper()
            wsId = __BotWorkspaceIDs__[destination_bot]
            if wsId == None:
                wsId = __BotWorkspaceIDs__["AGENT"]

            if destination_bot == None:
                destination_bot = "agent"
        except:
            logger.info(
            "User Query: {0}".format('exception in GetDestinationBot')
            )
            raise

        return wsId
    
    def __prepareWatsonContext__(self, context, additionalData):
        watsonContext = {}
        if "destination_bot" in context.Properties:
            watsonContext["destination_bot"] = context["destination_bot"]
        if "workspaceIds" in additionalData:
            watsonContext["workspaceIds"] = additionalData["workspaceIds"]
        
        return watsonContext

    def __init__(self):
        if not NLPBot.init:
            raise Exception("Initialize the bot before using it")

    def Process(self, request) -> BotOutput:
        logger.info(
            "User Query: {0}".format('in processor function')

        )
        engine = EngineInterface()
        logger.info(
            "User Query: {0}".format('engine intialized')

        )
        watsonRequest = {
            "context": self.__prepareWatsonContext__(request.Context, request.AdditionalData),
            "input": {
                "type": "text",
                "text": request.Input.Message
            }
        }
        swrequest = json.dumps(watsonRequest)
        logger.info(
            "User Query: {0}".format(swrequest)

        )

        try:
            start = datetime.datetime.now()
            resp = engine.Execute(watsonRequest)
            stop = datetime.datetime.now()
            totalTime="Total time used for execute watsonRequest :" + str(stop - start)
            logger.info(
            "User Query: {0}".format(totalTime)
            )
            logger.info(
                "User Query: {0}".format(('watson response'))

            )
            botOutput = BotOutput()
            botOutput.AdditionalData = request.AdditionalData
            respdata=resp
            data=request.AdditionalData
            isnlp= True
            insertIntentEntity(respdata,data,isnlp)
            
            botOutput.Entities = self.__extractEntities__(resp["entities"], watsonRequest["input"]["text"])
            botOutput.Intent = self.__extractIntent__(resp["intents"])
            botOutput.Context = self.__prepareOutputContext__(resp["context"], request.Context)
            logging.debug("Intents {botOutput.Intent.Name}")
            
        except:
            logger.info(
                "User Query: {0}".format(('an exception occurd in watson response'))

            )
            raise
        return botOutput
    
    def __prepareOutputContext__(self, watsonContext, originalContext):
        if "destination_bot" in watsonContext:
            originalContext.Properties["destination_bot"] = watsonContext["destination_bot"]
        return originalContext

    def __extractEntities__(self, watsonEntities, inputText):
        entities = []
        for entity in watsonEntities:
            e = Entity()
            e.Name = entity["entity"]
            e.Value = entity["value"]
            e.Confidence = entity["confidence"]
            location = entity["location"]
            if len(location) > 1:
                e.RawValue = inputText[location[0]:location[1]]
            entities.append(e)
        return entities
    
    def __extractIntent__(self, watsonIntents):
        if len(watsonIntents) == 0:
            return None

        sortedList = sorted(watsonIntents,key=self.__sortbyconfidence__,reverse=True)
        intent = Intent()
        intent.Name = sortedList[0]["intent"]
        intent.Confidence = sortedList[0]["confidence"]
        return intent

    def __sortbyconfidence__(self, elem):
        return elem["confidence"]
