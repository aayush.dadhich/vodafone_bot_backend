import json
from dataAccessLayer.dbHandler import insert_intent_entity_info

def insertIntentEntity(responsedata, data,isnlp):
    if 'intents' in responsedata:
        if len(responsedata['intents'])!=0:
            for intent_count in range(0,len(responsedata['intents'])):
                intent=responsedata['intents'][intent_count]['intent'] 
                confidence=responsedata['intents'][intent_count]['confidence']   
        else:
            intent="no intent found"
            confidence="no confidence found"
    else:
        intent="no intent found"
        confidence="no confidence found"

    if 'entities' in responsedata:
        if len(responsedata['entities'])!=0:
            entity=json.dumps(responsedata['entities'])
        else:
            entity=json.dumps("no entity found")
    else:
        entity=json.dumps("no entity found")
    if 'input' in responsedata and 'text' in responsedata['input']:
        message=responsedata['input']['text']
    else:
        message='no message found'

    if 'logId' in data:
        result=insert_intent_entity_info(data['logId'],message,intent,confidence,entity,isnlp)
    else: 
        result='query not executed'    
    return result

def insertIntentEntityConversationalBot(data,confidence,isnlp):
    if 'message' in data and 'data' in data['message'] and '#Name' in data['message']['data']:
        intent=data['message']['data']['#Name']
    else:
        intent='no intent found'
    entity=json.dumps("conversation bot")

    if 'message' in data and 'data' in data['message'] and '@text' in data['message']['data']:
        message=data['message']['data']['@text']
    else:
        message='no message found'
    if 'logId' in data:
        result=insert_intent_entity_info(data['logId'],message,intent,confidence,entity,isnlp)    
    else: 
        result='query not executed'    
    return result
