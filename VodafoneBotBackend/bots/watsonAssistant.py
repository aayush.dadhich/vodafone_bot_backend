from ibm_watson import AssistantV1
import logging
import json
logger = logging.getLogger(__name__)

class Assistant:
    # """ This is a class description""

    def __init__(self, config):
        self.__assistant__ = AssistantV1(iam_apikey= config.get('iam_apikey'),
                                        version= config.get('version'),
                                        url= config.get('url')
                                        )

    def Message(self, payload):

        try:
            response = self.__assistant__.message(workspace_id="78e53624-0429-4f1e-87fe-477f1ed98b3e",
                                                  input=payload["input"],
                                                  context=payload["context"]
                                                  ).get_result()

            logger.info(
                "User Query: {0}".format(response),
                extra=response
            )
        except:
            logger.error(
                "User Query: {0}".format(('error from watson end')),
                extra=response
            )                    
        return response
